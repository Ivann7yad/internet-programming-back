﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using InternetPropgrammingBack.BLL.Attributes;
using System.ComponentModel.DataAnnotations.Schema;

namespace ToDoList.BLL.DTO.ToDoTaskDTO
{
    public class StudentCreateDTO
    {
        [Required]
        [MaxLength(50)]
        [Display(Order = 2)]
        [Column(name: "First name")]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(150)]
        [Display(Order = 3)]
        [Column(name: "Last name")]
        public string LastName { get; set; }

        [Required]
        [MaxLength(150)]
        [Display(Order = 4)]
        [Column(name: "Group")]
        public string Group { get; set; }

        [Required]
        [Display(Order = 5)]
        [Column(name: "Birthday")]
        public string Birthday { get; set; }

        [Required]
        [Display(Order = 6)]
        public string Status { get; set; }

        [Required]
        [Display(Order = 7)]
        public string Gender { get; set; }

        [Required]
        [EmailAddress]
        [DefaultValue("")]
        public string Email { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "UserName maximum length is 50")]
        [DefaultValue("")]
        public string UserName { get; set; }

        [Required]
        [MaxLength(30, ErrorMessage = "Password maximum length is 30")]
        [DefaultValue("")]
        public string Password { get; set; }

        [Required]
        [Display(Name = "Confirm password")]
        [MaxLength(30, ErrorMessage = "Password maximum length is 30")]
        [DefaultValue("")]
        [Compare(nameof(Password))]
        public string PasswordConfirmation { get; set; }
    }
}
