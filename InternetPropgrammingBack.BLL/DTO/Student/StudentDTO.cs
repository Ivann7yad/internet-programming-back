﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace ToDoList.BLL.DTO.ToDoTaskDTO
{
    public class StudentDTO
    {
        [Required]
        public string Id { get; set; }

        [Required]
        [MaxLength(50)]
        [DisplayName("First name")]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(150)]
        [DisplayName("Last name")]
        public string LastName { get; set; }

        [Required]
        [MaxLength(10)]
        [DisplayName("Group")]
        public string Group { get; set; }

        [Required]
        [DisplayName("Birthday")]
        public string Birthday { get; set; }

        [Required]
        public string Status { get; set; }

        [Required]
        public string Gender { get; set; }
        
        [Required]
        public string Email { get; set; }

        [Required]
        public string UserName { get; set; }
    }
}
