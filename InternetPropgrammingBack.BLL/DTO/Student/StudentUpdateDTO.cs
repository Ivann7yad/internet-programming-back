﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using InternetPropgrammingBack.BLL.DTO;
using InternetPropgrammingBack.BLL.Attributes;

namespace ToDoList.BLL.DTO.ToDoTaskDTO
{
    public class StudentUpdateDTO: IUpdateDTO
    {
        [Required]
        public string Id { get; set; }

        [Required]
        [MinLength(2)]
        [MaxLength(50)]
        [DisplayName("First name")]
        [ValidName]
        public string FirstName { get; set; }

        [Required]
        [MinLength(2)]
        [MaxLength(150)]
        [DisplayName("Last name")]
        [ValidName]
        public string LastName { get; set; }

        [Required]
        [MaxLength(10)]
        [DisplayName("Group")]
        [DefaultValue("")]
        [ValidGroup]
        public string Group { get; set; }

        [Required]
        [DisplayName("Birthday")]
        [ValidBirthday]
        public string Birthday { get; set; }

        [Required]
        [AllowedValues("online", "offline")]
        public string Status { get; set; }

        [Required]
        [AllowedValues("male", "female")]
        public string Gender { get; set; }

        [Required]
        [EmailAddress]
        [DefaultValue("")]
        public string Email { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "UserName maximum length is 50")]
        [DefaultValue("")]
        public string UserName { get; set; }
    }
}
