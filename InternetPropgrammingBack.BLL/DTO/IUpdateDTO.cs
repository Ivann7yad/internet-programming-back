﻿using Azure.Core.Pipeline;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetPropgrammingBack.BLL.DTO
{
    public interface IUpdateDTO
    {
        public string Id { get; }
    }
}
