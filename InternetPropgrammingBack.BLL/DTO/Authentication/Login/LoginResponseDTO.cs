﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetPropgrammingBack.BLL.DTO.Authentication.Login
{
    public class LoginResponseDTO
    {
        public string UserId { get; set; }
        public string UserEmail { get; set; }
    }
}
