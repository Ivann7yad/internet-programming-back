﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetPropgrammingBack.BLL.DTO.Authentication.Login
{
    public class LoginRequestDTO
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
