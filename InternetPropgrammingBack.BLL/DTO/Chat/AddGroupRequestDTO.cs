﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetPropgrammingBack.BLL.DTO.Chat
{
    public class AddGroupRequestDTO
    {
        public string GroupName { get; set; }
        public string[] MembersIds { get; set; }
    }
}
