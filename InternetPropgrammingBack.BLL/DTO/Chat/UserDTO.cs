﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Threading.Tasks;

namespace InternetPropgrammingBack.BLL.DTO.Chat
{
    public class UserDTO
    {
        public string UserId { get; set; }
        public string UserEmail { get; set; }
    }
}
