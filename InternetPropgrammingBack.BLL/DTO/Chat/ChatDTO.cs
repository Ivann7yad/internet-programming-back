﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetPropgrammingBack.BLL.DTO.Chat
{
    public class ChatDTO
    {
        public string Id { get; set; }
        public string ChatName { get; set; }
    }
}
