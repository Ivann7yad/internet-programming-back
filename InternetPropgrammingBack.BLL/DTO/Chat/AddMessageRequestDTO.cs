﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetPropgrammingBack.BLL.DTO.Chat
{
    public class AddMessageRequestDTO
    {
        public string Message { get; set; }
        public string SenderId { get; set; }
        public string ConversationId { get; set; }

        [AllowedValues("group", "individual")]
        public string ConversationType { get; set; }
    }
}
