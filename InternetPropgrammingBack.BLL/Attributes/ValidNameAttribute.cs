﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace InternetPropgrammingBack.BLL.Attributes
{
    public class ValidNameAttribute : ValidationAttribute
    {
        protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
        {
            string? name = value as string;
            if (name is null)
            {
                return new ValidationResult("Value must be of type string");
            }

            bool noWhiteSpacesMatch = Regex.IsMatch(name, @"^\b\S+\b$", RegexOptions.None, TimeSpan.FromMilliseconds(100));

            if (!noWhiteSpacesMatch)
            {
                return new ValidationResult("First/Last name must contain no whiespaces");
            }

            bool commonNameMatch = Regex.IsMatch(name, @"^\b[A-Za-z]*\b$", RegexOptions.None, TimeSpan.FromMilliseconds(100));
            bool hyphenNameMatch = Regex.IsMatch(name, @"^\b[A-Za-z]*-[A-Za-z]*\b$", RegexOptions.None, TimeSpan.FromMilliseconds(100));

            if (!(commonNameMatch || hyphenNameMatch))
            {
                return new ValidationResult("First/Last name must contain only latin letters and '-'");
            }

            bool firstLetterCapitalMatch = Regex.IsMatch(name, @"^\b[A-Z]\w*\b$", RegexOptions.None, TimeSpan.FromMilliseconds(100));
            bool firstLetterCapitalHyphenMatch = Regex.IsMatch(name, @"^\b[A-Z][a-z]*-[A-Z][a-z]*\b$", RegexOptions.None, TimeSpan.FromMilliseconds(100));

            if (!(firstLetterCapitalMatch || firstLetterCapitalHyphenMatch))
            {
                return new ValidationResult("First/Last name must contain only first letter capital");
            }

            return ValidationResult.Success;
        }
    }
}
