﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace InternetPropgrammingBack.BLL.Attributes
{
    public class ValidGroupAttribute : ValidationAttribute
    {
        protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
        {
            string? name = value as string;
            if (name is null)
            {
                return new ValidationResult("Value must be of type string");
            }

            bool noWhiteSpacesMatch = Regex.IsMatch(name, @"^\b\S+\b$", RegexOptions.None, TimeSpan.FromMilliseconds(100));
            
            if (!noWhiteSpacesMatch)
            {
                return new ValidationResult("Group name must contain no whiespaces");
            }

            bool validGroupNameMatch = Regex.IsMatch(name, @"^\b[A-Z]*-\d*\b$", RegexOptions.None, TimeSpan.FromMilliseconds(100));
            
            if (!validGroupNameMatch)
            {
                return new ValidationResult("Invalid group format");
            }

            return ValidationResult.Success;
        }
    }
}
