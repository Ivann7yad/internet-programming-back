﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace InternetPropgrammingBack.BLL.Attributes
{
    public class ValidBirthdayAttribute : ValidationAttribute
    {
        protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
        {
            DateOnly? birthday = DateOnly.Parse((value as string) ?? "");
            if (birthday is null)
            {
                return new ValidationResult("Value must be of type DateOnly");
            }

            if (birthday > DateOnly.Parse("2007-01-01"))
            {
                return new ValidationResult("Birthday cannot be later than 01/01/2007");
            }

            if (birthday < DateOnly.Parse("1945-01-01"))
            {
                return new ValidationResult("Birthday cannot be earlier than 01/01/1945");
            }

            return ValidationResult.Success;
        }
    }
}
