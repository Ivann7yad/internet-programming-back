﻿using InternetPropgrammingBack.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoList.BLL.DTO.ToDoTaskDTO;

namespace InternetPropgrammingBack.BLL.Services.Validators.Interfaces
{
    public interface IBaseValidator<TCreateDTO, TUpdateDTO>
        where TUpdateDTO : IUpdateDTO
    {
        bool IsUpdateDTOValid(string id, TUpdateDTO? updateDTO);
        bool IsCreateDTOValid(TCreateDTO? createDTO);
    }
}
