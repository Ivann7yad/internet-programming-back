﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoList.BLL.DTO.ToDoTaskDTO;

namespace InternetPropgrammingBack.BLL.Services.Validators.Interfaces
{
    public interface IStudentValidator : IBaseValidator<StudentCreateDTO, StudentUpdateDTO>
    {
    }
}
