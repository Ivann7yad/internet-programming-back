﻿using InternetPropgrammingBack.BLL.DTO;
using InternetPropgrammingBack.BLL.Services.Validators.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetPropgrammingBack.BLL.Services.Validators
{
    public class BaseValidator<TCreateDTO, TUpdateDTO> : IBaseValidator<TCreateDTO, TUpdateDTO>
        where TUpdateDTO : IUpdateDTO
    {
        public bool IsCreateDTOValid(TCreateDTO? createDTO)
        {
            return createDTO is null ? false : true;
        }

        public bool IsUpdateDTOValid(string id, TUpdateDTO? updateDTO)
        {
            if (updateDTO is null || id != updateDTO.Id)
                return false;

            return true;
        }
    }
}
