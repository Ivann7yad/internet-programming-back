﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoList.BLL.DTO.ToDoTaskDTO;
using ToDoList.DAL.Entity;

namespace InternetPropgrammingBack.BLL.Mapping.ToDoTaskMapping
{
    public class StudentMapper: Profile
    {
        public StudentMapper()
        {
            CreateMap<Student, StudentDTO>()
                .ForMember(st => st.Birthday, dto => dto.MapFrom(st => st.Birthday.ToString()));
            CreateMap<StudentCreateDTO, Student>()
                .ForMember(st => st.Birthday, dto => dto.MapFrom(st => DateOnly.Parse(st.Birthday)));
            CreateMap<StudentUpdateDTO, Student>()
                .ForMember(st => st.Birthday, dto => dto.MapFrom(st => DateOnly.Parse(st.Birthday)));
            
        }
    }
}
