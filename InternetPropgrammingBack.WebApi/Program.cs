using Microsoft.EntityFrameworkCore;
using ToDoList.DAL.Data;
using ToDoList.DAL.Repository.Interfaces;
using ToDoList.DAL.Repository;
using ToDoList.BLL.Mapping.ToDoTaskMapping;
using InternetPropgrammingBack.DAL.Repository;
using InternetPropgrammingBack.DAL.Repository.Interfaces;
using InternetPropgrammingBack.BLL.Mapping.ToDoTaskMapping;
using Microsoft.AspNetCore.Identity;
using InternetPropgrammingBack.DAL.Entity;
using InternetPropgrammingBack.WebApi.Hubs;
using InternetPropgrammingBack.DAL.Utils;
using Microsoft.Extensions.Options;
using ToDoList.DAL.Entity;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// Add ApplicationDbContext to services.
builder.Services.AddDbContext<ApplicationDbContext>(options =>
{
    options.UseSqlServer(builder.Configuration.GetConnectionString("Default"));
});

builder.Services.AddIdentity<Student, IdentityRole<string>>()
    .AddEntityFrameworkStores<ApplicationDbContext>();

builder.Services.Configure<MongoDbSettings>(builder.Configuration.GetSection(nameof(MongoDbSettings)));
builder.Services.AddSingleton<IMongoDbSettings>(sp => sp.GetRequiredService<IOptions<MongoDbSettings>>().Value);

// Add ToDoListRepository to services.
builder.Services.AddScoped<IToDoTaskRepository, ToDoTaskRepository>();
builder.Services.AddScoped<IStudentRepository, StudentRepository>();
builder.Services.AddSignalR();

// Add mapper.
builder.Services.AddAutoMapper(typeof(ToDoTaskMapper), typeof(StudentMapper));

// Use cors.
builder.Services.AddCors();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.UseCors(options =>
{
    options
        .WithOrigins("http://localhost:3000")
        .AllowAnyMethod()
        .AllowAnyHeader()
        .AllowCredentials();
});

app.MapControllers();
app.MapHub<ChatHub>("/hubs/chat");

app.Run();
