﻿using InternetPropgrammingBack.BLL.DTO.Chat;
using InternetPropgrammingBack.DAL.Entity;
using InternetPropgrammingBack.DAL.Utils;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using MongoDB.Driver;
using System.Runtime.CompilerServices;
using ToDoList.DAL.Data;

namespace InternetPropgrammingBack.WebApi.Hubs
{
    public class ChatHub : Hub
    {
        private readonly ApplicationDbContext _context;
        private readonly IMongoCollection<IndividualChat> _individualChats;
        private readonly IMongoCollection<Group> _groups;
        private readonly IMongoCollection<ChatMessage> _messages;
        private static readonly Dictionary<string, List<string>> _userConnections = new Dictionary<string, List<string>>();

        public ChatHub(ApplicationDbContext context, IMongoDbSettings mongoDbSettings)
        {
            _context = context;
            var client = new MongoClient(mongoDbSettings.ConnectionString);
            var database = client.GetDatabase(mongoDbSettings.DatabaseName);
            _groups = database.GetCollection<Group>(mongoDbSettings.GroupsCollectionName);
            _individualChats = database.GetCollection<IndividualChat>(mongoDbSettings.IndividualChatsCollectionName);
            _messages = database.GetCollection<ChatMessage>(mongoDbSettings.MessagesCollectionName);
        }

        public async Task<LoadConversationsResponse> OnConnectAndLoadConversations(string userId)
        {
            if (!_userConnections.Keys.Contains(userId))
            {
                _userConnections.Add(userId, [Context.ConnectionId]);
            }
            else
            {
                _userConnections[userId].Add(Context.ConnectionId);
            }

            var groups = _groups
                .Find(g => true)
                .ToList()
                .Where(group => group.Members.Any(user => user.Id == userId))
                .Select(group => new ChatDTO() { Id = group.Id, ChatName = group.GroupName });
            var individualChats = _individualChats
                .Find(ic => true)
                .ToList()
                .Where(indChat => indChat.Members.Any(user => user.Id == userId))
                .Select(indChat => GetIndividualChat(indChat, userId));

            var user = _context.Users.FirstOrDefault(u => u.Id == userId);
            if (user is not null)
            {
                user.Status = "online";
                _context.SaveChanges();
            }

            return new LoadConversationsResponse()
            {
                Groups = groups,
                IndividualChats = individualChats,
            };
        }

        public async Task AddNewGroup(AddGroupRequestDTO addGroupRequestDTO)
        {
            var members = _context.Users
                .Where(user => addGroupRequestDTO.MembersIds.Any(id => id == user.Id))
                .ToList();

            var newGroup = new Group()
            {
                GroupName = addGroupRequestDTO.GroupName,
                Members = members,
            };

            _groups.InsertOne(newGroup);

            ChatDTO chatDTO = new ChatDTO()
            {
                Id = newGroup.Id,
                ChatName = newGroup.GroupName,
            };

            var connectionsToNotify = _userConnections
                .Where(kvp => addGroupRequestDTO.MembersIds.Contains(kvp.Key))
                .SelectMany(kvp => kvp.Value);

            await Clients.Clients(connectionsToNotify).SendAsync("ReceiveAddGroupNotification", chatDTO);
        }

        public async Task AddNewIndividualChat(AddIndividualChatRequestDTO addIndividualChatRequestDTO)
        {
            var members = _context.Users
                .Where(user => addIndividualChatRequestDTO.MembersIds.Any(id => id == user.Id))
                .ToList();

            var newIndividualChat = new IndividualChat()
            {
                Members = members,
            };

            _individualChats.InsertOne(newIndividualChat);
            _context.SaveChanges();

            string userIdOne = addIndividualChatRequestDTO.MembersIds[0];
            var chatOneDTO = GetIndividualChat(newIndividualChat, userIdOne);
            var connectionsOneToNotify = _userConnections
                .Where(usrCon => usrCon.Key == addIndividualChatRequestDTO.MembersIds[0])
                .SelectMany(kvp => kvp.Value);

            string userIdTwo = addIndividualChatRequestDTO.MembersIds[1];
            var chatTwoDTO = GetIndividualChat(newIndividualChat, userIdTwo);
            var connectionsTwoToNotify = _userConnections
                .Where(usrCon => usrCon.Key == addIndividualChatRequestDTO.MembersIds[1])
                .SelectMany(kvp => kvp.Value);

            await Clients.Clients(connectionsOneToNotify).SendAsync("ReceiveAddIndividualChatNotification", chatOneDTO);
            await Clients.Clients(connectionsTwoToNotify).SendAsync("ReceiveAddIndividualChatNotification", chatTwoDTO);
        }

        public async Task SendGroupMessage(AddMessageRequestDTO addMessageRequest)
        {
            ChatMessage chatMessage = new ChatMessage()
            {
                Message = addMessageRequest.Message,
                SenderId = addMessageRequest.SenderId,
                ConversationId = addMessageRequest.ConversationId,
                ConversationType = addMessageRequest.ConversationType,
            };

            _messages.InsertOne(chatMessage);
            _context.SaveChanges();

            var membersIds = _groups?
                .Find(g => true)?
                .ToList()?
                .FirstOrDefault(group => group.Id == addMessageRequest.ConversationId)?
                .Members
                .Select(user => user.Id)
                .ToList();

            var connectionsId = _userConnections
                .Where(usrConn => membersIds.Any(memberId => memberId == usrConn.Key))
                .SelectMany(usrConn => usrConn.Value);
            await Clients.Clients(connectionsId).SendAsync("ReceiveGroupMessage", chatMessage);
        }

        public async Task SendIndividualMessage(AddMessageRequestDTO addMessageRequest)
        {
            ChatMessage chatMessage = new ChatMessage()
            {
                Message = addMessageRequest.Message,
                SenderId = addMessageRequest.SenderId,
                ConversationId = addMessageRequest.ConversationId,
                ConversationType = addMessageRequest.ConversationType,
            };

            _messages.InsertOne(chatMessage);
            _context.SaveChanges();

            var membersIds = _individualChats
                .Find(g => true)?
                .ToList()?
                .FirstOrDefault(indChat => indChat.Id == addMessageRequest.ConversationId)?
                .Members
                .Select(user => user.Id)
                .ToList();

            var connectionsId = _userConnections
                .Where(usrConn => membersIds.Any(memberId => memberId == usrConn.Key))
                .SelectMany(usrConn => usrConn.Value);
            await Clients.Clients(connectionsId).SendAsync("ReceiveIndividualChatMessage", chatMessage);
        }

        public async Task CloseConnection(string userId)
        {
            if (!_userConnections.ContainsKey(userId))
            {
                return;
            }

            if (_userConnections[userId].Contains(Context.ConnectionId))
            {
                _userConnections[userId].Remove(Context.ConnectionId);
            }

            var user = _context.Users.FirstOrDefault(u => u.Id == userId);
            if (user is not null)
            {
                user.Status = "offline";
                _context.SaveChanges();
            }
        }

        private static ChatDTO GetIndividualChat(IndividualChat individualChat, string userId)
        {
            var otherUser = individualChat.Members.First(members => members.Id != userId).Email;
            return new ChatDTO() { Id = individualChat.Id, ChatName = otherUser };
        }
    }

    public class LoadConversationsResponse
    {
        public IEnumerable<ChatDTO> Groups { get; set; }
        public IEnumerable<ChatDTO> IndividualChats { get; set; }
    }
}
