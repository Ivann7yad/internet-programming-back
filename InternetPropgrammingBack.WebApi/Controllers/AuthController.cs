﻿using Azure.Core;
using InternetPropgrammingBack.BLL.DTO.Authentication.Login;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration.UserSecrets;
using System.IdentityModel.Tokens.Jwt;
using ToDoList.DAL.Data;
using ToDoList.DAL.Entity;

namespace InternetPropgrammingBack.WebApi.Controllers
{
    [ApiController]
    public class AuthController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<Student> _userManager;
        
        public AuthController(ApplicationDbContext context, UserManager<Student> userManager, RoleManager<IdentityRole<string>> roleManager)
        {
            this._context = context;
            this._userManager = userManager;
        }

        [HttpPost]
        [Route("api/auth/login")]
        public async Task<IActionResult> Login([FromBody] LoginRequestDTO loginDTO)
        {
            var user = _context.Users.FirstOrDefault(userFromDb => userFromDb.Email == loginDTO.Login);
            bool isUserNull = user is null;

            bool isValid = isUserNull ? false : await _userManager.CheckPasswordAsync(user !, loginDTO.Password);
            if (isValid)
            {
                LoginResponseDTO loginResponseDTO = new LoginResponseDTO()
                {
                    UserEmail = user.Email,
                    UserId = user.Id
                };
                return Ok(loginResponseDTO);
            }

            return BadRequest("Invalid 'login' or 'password'");
        }
    }
}
