﻿using InternetPropgrammingBack.BLL.DTO.Authentication.Login;
using InternetPropgrammingBack.BLL.DTO.Chat;
using InternetPropgrammingBack.DAL.Entity;
using InternetPropgrammingBack.DAL.Utils;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using ToDoList.DAL.Data;

namespace InternetPropgrammingBack.WebApi.Controllers
{
    [ApiController]
    public class UserController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IMongoCollection<IndividualChat> _individualChats;
        private readonly IMongoCollection<Group> _groups;
        private readonly IMongoCollection<ChatMessage> _messages;

        public UserController(ApplicationDbContext context, IMongoDbSettings mongoDbSettings)
        {
            this._context = context;
            var client = new MongoClient(mongoDbSettings.ConnectionString);
            var database = client.GetDatabase(mongoDbSettings.DatabaseName);
            _groups = database.GetCollection<Group>(mongoDbSettings.GroupsCollectionName);
            _individualChats = database.GetCollection<IndividualChat>(mongoDbSettings.IndividualChatsCollectionName);
            _messages = database.GetCollection<ChatMessage>(mongoDbSettings.MessagesCollectionName);
        }

        [HttpGet]
        [Route("api/Users/GetAllForGroup")]
        public async Task<IActionResult> GetAllForGroup([FromQuery] string userId)
        {
            var users = _context.Users
                .Where(user => user.Id != userId)
                .Select(user => new UserDTO()
            {
                UserEmail = user.Email,
                UserId = user.Id,
            }).ToList() ?? new List<UserDTO>();
            
            return Ok(users);
        }

        [HttpGet]
        [Route("api/Users/GetAllForIndividualChat")]
        public async Task<IActionResult> GetAllForIndividualChat([FromQuery] string userId)
        {
            var individualChatsOtherUsers = _individualChats
                .Find(chat => chat.Members.Any(user => user.Id == userId))
                .ToList()
                .Select(chat => chat.Members.FirstOrDefault(user => user.Id != userId))
                .ToList();

            if (individualChatsOtherUsers is null || !individualChatsOtherUsers.Any())
            {
                var usersToReturn = _context.Users
                    .Where(user => user.Id != userId)
                    .Select(user => new UserDTO()
                    {
                        UserEmail = user.Email,
                        UserId = user.Id,
                    }).ToList();
                return Ok(usersToReturn);
            }

            var users = _context.Users
                .Where(user => user.Id != userId)
                .AsEnumerable()
                .Where(user => individualChatsOtherUsers.FirstOrDefault(chatUser => chatUser.Id == user.Id) == null)
                .Select(user => new UserDTO()
                {
                    UserEmail = user.Email,
                    UserId = user.Id,
                }).ToList() ?? new List<UserDTO>();

            return Ok(users);
        }

        [HttpGet]
        [Route("api/Users/LoadMessages")]
        public async Task<IActionResult> LoadMessages([FromQuery] string conversationType, [FromQuery] string conversationId)
        {
            var messages = _messages
                .Find(message => message.ConversationId == conversationId && message.ConversationType == conversationType)
                .ToList();
            return Ok(messages);
        }
    }
}
