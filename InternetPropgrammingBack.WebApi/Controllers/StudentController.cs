﻿using AutoMapper;
using InternetPropgrammingBack.BLL.Services.Validators;
using InternetPropgrammingBack.BLL.Services.Validators.Interfaces;
using InternetPropgrammingBack.DAL.Repository.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Net;
using ToDoList.BLL.DTO.ToDoTaskDTO;
using ToDoList.BLL.GeneralTypes;
using ToDoList.DAL.Entity;

namespace InternetPropgrammingBack.WebApi.Controllers
{
    [ApiController]
    [Route("api/students")]
    public class StudentController : Controller
    {
        protected APIResponse _response;
        private readonly IStudentRepository _studentRepository;
        private readonly IMapper _mapper;
        private readonly IStudentValidator _validator;
        private readonly UserManager<Student> _userManager;
        public StudentController(IStudentRepository toDoTaskRepository, IMapper mapper, UserManager<Student> userManager)
        {
            _studentRepository = toDoTaskRepository;
            _mapper = mapper;
            _response = new() { ErrorMessages = new List<string>() };
            _validator = new StudentValidator();
            _userManager = userManager;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<APIResponse> GetStudents()
        {
            try
            {
                var tasks = _studentRepository.GetAll();
                var toDoTaskDTOs = _mapper.Map<List<StudentDTO>>(tasks);

                _response.Generate200OK(toDoTaskDTOs);
                return Ok(_response);
            }
            catch (Exception ex)
            {
                // In case of some exception, produce failing response.
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }

            return _response;
        }

        [HttpGet("{page:int}/{pageSize:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<APIResponse> GetStudentsPagination(int page = 1, int pageSize = 5)
        {
            try
            {
                var tasks = _studentRepository.GetAll(page, pageSize);
                var toDoTaskDTOs = _mapper.Map<List<StudentDTO>>(tasks);

                _response.Generate200OK(toDoTaskDTOs);
                return Ok(_response);
            }
            catch (Exception ex)
            {
                // In case of some exception, produce failing response.
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }

            return _response;
        }

        [HttpGet("{id}",Name = "GetStudent")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<APIResponse> GetStudent([FromRoute] string id)
        {
            try
            {
                var task = _studentRepository.Get(v => v.Id == id, tracked: false);
                if (task == null)
                {
                    string error = $"Entity with Id {id} not found";
                    _response.Generate404NotFound(error);
                    return BadRequest(_response);
                }

                var toDoTaskDTO = _mapper.Map<StudentDTO>(task);

                _response.Generate200OK(toDoTaskDTO);
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }

            return _response;
        }

        [HttpPost(Name = "CreateStudent")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<APIResponse>> CreateStudent([FromBody] StudentCreateDTO createDTO)
        {
            try
            {
                if (!_validator.IsCreateDTOValid(createDTO))
                {
                    string error = "Argument is null";
                    _response.Generate400BadRequest(error);
                    return BadRequest(_response);
                }

                // If everything is ok, save student in database and produce response with code 201.
                var student = _mapper.Map<Student>(createDTO);
                string password = createDTO.Password;
                if (!await IsUserUnique(student))
                {
                    return BadRequest("User is not unique");
                }

                await _userManager.CreateAsync(student, password);

                _response.Result = _mapper.Map<StudentDTO>(student);
                _response.StatusCode = HttpStatusCode.Created;
                return CreatedAtRoute(nameof(GetStudent), new { id = student.Id }, _response);
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }

            return _response;
        }

        [HttpDelete("{id}", Name = "DeleteStudent")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<APIResponse>> DeleteStudent([FromRoute]string id)
        {
            try
            {
                var student = _studentRepository.Get(v => v.Id == id);
                if (student == null)
                {
                    string error = $"Entity with Id {id} not found";
                    _response.Generate404NotFound(error);
                    return NotFound(_response);
                }

                await _userManager.DeleteAsync(student);

                _response.Generate204NoContent();
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }

            return _response;
        }

        [HttpPut("{id}", Name = "UpdateStudent")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<APIResponse> UpdateStudent([FromRoute]string id, [FromBody] StudentUpdateDTO updateDTO)
        {
            try
            {
                if (!_validator.IsUpdateDTOValid(id, updateDTO))
                {
                    string error = string.Empty;
                    if (updateDTO is null)
                        error = "Argument is null";
                    else
                        error = "Input Id and Id in body must match";

                    _response.Generate400BadRequest(error);
                    return BadRequest(_response);
                }

                var studentFromDb = _studentRepository.Get(v => v.Id == id, tracked: false);
                if (studentFromDb == null)
                {
                    string error = $"Entity with Id {id} not found";
                    _response.Generate404NotFound(error);
                    return NotFound(_response);
                }

                studentFromDb = _mapper.Map<Student>(updateDTO);
                
                _studentRepository.Update(studentFromDb);
                _studentRepository.Save();

                var studentDTO = _mapper.Map<StudentDTO>(studentFromDb);
                _response.Generate200OK(studentDTO);
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() { ex.ToString() };
            }

            return _response;
        }

        private async Task<bool> IsUserUnique(Student user)
        {
            // Check if user is unique by email or username.
            var userFromDbDyEmail = await _userManager.FindByEmailAsync(user.Email);

            if (userFromDbDyEmail is not null)
            {
                return false;
            }

            return true;
        }
    }
}
