﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace InternetPropgrammingBack.DAL.Entity
{
    public class ChatMessage
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        public string Message { get; set; }
        public string SenderId { get; set; }
        public string ConversationId { get; set; }

        [AllowedValues("group", "individual")]
        public string ConversationType { get; set; }
    }
}
