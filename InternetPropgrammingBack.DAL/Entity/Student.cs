﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ToDoList.DAL.Entity
{
    [Table(name: "Students")]
    public class Student : IdentityUser
    {
        [Required]
        [MaxLength(50)]
        [Display(Order = 2)]
        [Column(name: "First name")]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(150)]
        [Display(Order = 3)]
        [Column(name: "Last name")]
        public string LastName { get; set; }

        [Required]
        [MaxLength(150)]
        [Display(Order = 4)]
        [Column(name: "Group")]
        public string Group { get; set; }

        [Required]
        [Display(Order = 5)]
        [Column(name: "Birthday")]
        public DateOnly Birthday { get; set; }

        [Required]
        [Display(Order = 6)]
        public string Status { get; set; }
        
        [Required]
        [Display(Order = 7)]
        public string Gender { get; set; }
    }
}
