﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace InternetPropgrammingBack.DAL.Migrations
{
    /// <inheritdoc />
    public partial class ChangeDateTimeToDateOnlyInStudentBirthday : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateOnly>(
                name: "Birthday",
                table: "Students",
                type: "date",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.UpdateData(
                table: "Students",
                keyColumn: "Id",
                keyValue: 1,
                column: "Birthday",
                value: new DateOnly(2000, 7, 7));

            migrationBuilder.UpdateData(
                table: "Students",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Birthday", "Gender" },
                values: new object[] { new DateOnly(2004, 11, 22), "female" });

            migrationBuilder.UpdateData(
                table: "Students",
                keyColumn: "Id",
                keyValue: 3,
                column: "Birthday",
                value: new DateOnly(1991, 7, 7));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 3, 20, 18, 22, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 3, 19, 18, 22, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 3, 20, 18, 22, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 3, 19, 18, 22, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 3, 20, 18, 22, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 3, 19, 18, 22, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 3, 20, 18, 22, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 3, 19, 18, 22, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 3, 20, 18, 22, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 3, 19, 18, 22, 1, 0, DateTimeKind.Unspecified) });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "Birthday",
                table: "Students",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateOnly),
                oldType: "date");

            migrationBuilder.UpdateData(
                table: "Students",
                keyColumn: "Id",
                keyValue: 1,
                column: "Birthday",
                value: new DateTime(2000, 7, 7, 14, 47, 1, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "Students",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Birthday", "Gender" },
                values: new object[] { new DateTime(2004, 11, 22, 14, 47, 1, 0, DateTimeKind.Unspecified), "Female" });

            migrationBuilder.UpdateData(
                table: "Students",
                keyColumn: "Id",
                keyValue: 3,
                column: "Birthday",
                value: new DateTime(1991, 7, 7, 14, 47, 1, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 2, 28, 14, 47, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 2, 27, 14, 47, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 2, 28, 14, 47, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 2, 27, 14, 47, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 2, 28, 14, 47, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 2, 27, 14, 47, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 2, 28, 14, 47, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 2, 27, 14, 47, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 2, 28, 14, 47, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 2, 27, 14, 47, 1, 0, DateTimeKind.Unspecified) });
        }
    }
}
