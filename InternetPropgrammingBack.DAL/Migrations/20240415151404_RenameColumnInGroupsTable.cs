﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace InternetPropgrammingBack.DAL.Migrations
{
    /// <inheritdoc />
    public partial class RenameColumnInGroupsTable : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "First name",
                table: "Groups",
                newName: "Group name");

            migrationBuilder.AddColumn<string>(
                name: "ConversationType",
                table: "ChatMessages",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 16, 18, 14, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 15, 18, 14, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 16, 18, 14, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 15, 18, 14, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 16, 18, 14, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 15, 18, 14, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 16, 18, 14, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 15, 18, 14, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 16, 18, 14, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 15, 18, 14, 1, 0, DateTimeKind.Unspecified) });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ConversationType",
                table: "ChatMessages");

            migrationBuilder.RenameColumn(
                name: "Group name",
                table: "Groups",
                newName: "First name");

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 14, 21, 26, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 13, 21, 26, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 14, 21, 26, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 13, 21, 26, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 14, 21, 26, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 13, 21, 26, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 14, 21, 26, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 13, 21, 26, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 14, 21, 26, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 13, 21, 26, 1, 0, DateTimeKind.Unspecified) });
        }
    }
}
