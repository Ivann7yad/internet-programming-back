﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace InternetPropgrammingBack.DAL.Migrations
{
    /// <inheritdoc />
    public partial class AddChatRelatedTables1 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GroupUser1");

            migrationBuilder.DropTable(
                name: "IndividualChatUser1");

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 14, 21, 19, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 13, 21, 19, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 14, 21, 19, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 13, 21, 19, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 14, 21, 19, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 13, 21, 19, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 14, 21, 19, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 13, 21, 19, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 14, 21, 19, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 13, 21, 19, 1, 0, DateTimeKind.Unspecified) });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GroupUser1",
                columns: table => new
                {
                    GroupId = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupUser1", x => new { x.GroupId, x.UserId });
                    table.ForeignKey(
                        name: "FK_GroupUser1_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GroupUser1_Groups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "IndividualChatUser1",
                columns: table => new
                {
                    IndividualChatId = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IndividualChatUser1", x => new { x.IndividualChatId, x.UserId });
                    table.ForeignKey(
                        name: "FK_IndividualChatUser1_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_IndividualChatUser1_IndividualChats_IndividualChatId",
                        column: x => x.IndividualChatId,
                        principalTable: "IndividualChats",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 14, 21, 14, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 13, 21, 14, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 14, 21, 14, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 13, 21, 14, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 14, 21, 14, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 13, 21, 14, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 14, 21, 14, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 13, 21, 14, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 14, 21, 14, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 13, 21, 14, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.CreateIndex(
                name: "IX_GroupUser1_UserId",
                table: "GroupUser1",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_IndividualChatUser1_UserId",
                table: "IndividualChatUser1",
                column: "UserId");
        }
    }
}
