﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace InternetPropgrammingBack.DAL.Migrations
{
    /// <inheritdoc />
    public partial class AddChatRelatedTables : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "AspNetUsers",
                type: "nvarchar(13)",
                maxLength: 13,
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateTable(
                name: "ChatMessages",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Message = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    SenderId = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ConversationId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChatMessages", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Groups",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Firstname = table.Column<string>(name: "First name", type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Groups", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "IndividualChats",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IndividualChats", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GroupUser",
                columns: table => new
                {
                    JoinedGroupsId = table.Column<int>(type: "int", nullable: false),
                    MembersId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupUser", x => new { x.JoinedGroupsId, x.MembersId });
                    table.ForeignKey(
                        name: "FK_GroupUser_AspNetUsers_MembersId",
                        column: x => x.MembersId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GroupUser_Groups_JoinedGroupsId",
                        column: x => x.JoinedGroupsId,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GroupUser1",
                columns: table => new
                {
                    GroupId = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupUser1", x => new { x.GroupId, x.UserId });
                    table.ForeignKey(
                        name: "FK_GroupUser1_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GroupUser1_Groups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "IndividualChatUser",
                columns: table => new
                {
                    IndividualConversationsId = table.Column<int>(type: "int", nullable: false),
                    MembersId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IndividualChatUser", x => new { x.IndividualConversationsId, x.MembersId });
                    table.ForeignKey(
                        name: "FK_IndividualChatUser_AspNetUsers_MembersId",
                        column: x => x.MembersId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_IndividualChatUser_IndividualChats_IndividualConversationsId",
                        column: x => x.IndividualConversationsId,
                        principalTable: "IndividualChats",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "IndividualChatUser1",
                columns: table => new
                {
                    IndividualChatId = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IndividualChatUser1", x => new { x.IndividualChatId, x.UserId });
                    table.ForeignKey(
                        name: "FK_IndividualChatUser1_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_IndividualChatUser1_IndividualChats_IndividualChatId",
                        column: x => x.IndividualChatId,
                        principalTable: "IndividualChats",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 14, 21, 14, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 13, 21, 14, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 14, 21, 14, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 13, 21, 14, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 14, 21, 14, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 13, 21, 14, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 14, 21, 14, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 13, 21, 14, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 14, 21, 14, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 13, 21, 14, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.CreateIndex(
                name: "IX_GroupUser_MembersId",
                table: "GroupUser",
                column: "MembersId");

            migrationBuilder.CreateIndex(
                name: "IX_GroupUser1_UserId",
                table: "GroupUser1",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_IndividualChatUser_MembersId",
                table: "IndividualChatUser",
                column: "MembersId");

            migrationBuilder.CreateIndex(
                name: "IX_IndividualChatUser1_UserId",
                table: "IndividualChatUser1",
                column: "UserId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ChatMessages");

            migrationBuilder.DropTable(
                name: "GroupUser");

            migrationBuilder.DropTable(
                name: "GroupUser1");

            migrationBuilder.DropTable(
                name: "IndividualChatUser");

            migrationBuilder.DropTable(
                name: "IndividualChatUser1");

            migrationBuilder.DropTable(
                name: "Groups");

            migrationBuilder.DropTable(
                name: "IndividualChats");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "AspNetUsers");

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 14, 16, 12, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 13, 16, 12, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 14, 16, 12, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 13, 16, 12, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 14, 16, 12, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 13, 16, 12, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 14, 16, 12, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 13, 16, 12, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 14, 16, 12, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 13, 16, 12, 1, 0, DateTimeKind.Unspecified) });
        }
    }
}
