﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace InternetPropgrammingBack.DAL.Migrations
{
    /// <inheritdoc />
    public partial class InitialMigration : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Students",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Firstname = table.Column<string>(name: "First name", type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Lastname = table.Column<string>(name: "Last name", type: "nvarchar(150)", maxLength: 150, nullable: false),
                    Group = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    Birthday = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Status = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Gender = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Students", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Tasks",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    Starttime = table.Column<DateTime>(name: "Start time", type: "datetime2", nullable: false),
                    Endtime = table.Column<DateTime>(name: "End time", type: "datetime2", nullable: false),
                    Status = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Type = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tasks", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Students",
                columns: new[] { "Id", "Birthday", "First name", "Gender", "Group", "Last name", "Status" },
                values: new object[,]
                {
                    { 1, new DateTime(2000, 7, 7, 14, 47, 1, 0, DateTimeKind.Unspecified), "James", "male", "PZ-27", "Bond", "alive" },
                    { 2, new DateTime(2004, 11, 22, 14, 47, 1, 0, DateTimeKind.Unspecified), "Ann", "Female", "AI-11", "Bond", "alive" },
                    { 3, new DateTime(1991, 7, 7, 14, 47, 1, 0, DateTimeKind.Unspecified), "Walter", "male", "KN-88", "White", "dead" }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "Description", "Status", "End time", "Start time", "Title", "Type" },
                values: new object[,]
                {
                    { 1, "Just do nothing", "done", new DateTime(2024, 2, 28, 14, 47, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 2, 27, 14, 47, 1, 0, DateTimeKind.Unspecified), "Do nothing1", "feature" },
                    { 2, "Just do nothing", "in-progress", new DateTime(2024, 2, 28, 14, 47, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 2, 27, 14, 47, 1, 0, DateTimeKind.Unspecified), "Do nothing2", "bug" },
                    { 3, "Just do nothing", "in-progress", new DateTime(2024, 2, 28, 14, 47, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 2, 27, 14, 47, 1, 0, DateTimeKind.Unspecified), "Do nothing3", "feature" },
                    { 4, "Just do nothing", "to-do", new DateTime(2024, 2, 28, 14, 47, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 2, 27, 14, 47, 1, 0, DateTimeKind.Unspecified), "Do nothing4", "bug" },
                    { 5, "Just do nothing", "to-do", new DateTime(2024, 2, 28, 14, 47, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 2, 27, 14, 47, 1, 0, DateTimeKind.Unspecified), "Do nothing5", "feature" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Students");

            migrationBuilder.DropTable(
                name: "Tasks");
        }
    }
}
