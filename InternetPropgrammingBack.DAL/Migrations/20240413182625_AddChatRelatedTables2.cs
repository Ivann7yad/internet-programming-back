﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace InternetPropgrammingBack.DAL.Migrations
{
    /// <inheritdoc />
    public partial class AddChatRelatedTables2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "AspNetUsers");

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 14, 21, 26, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 13, 21, 26, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 14, 21, 26, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 13, 21, 26, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 14, 21, 26, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 13, 21, 26, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 14, 21, 26, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 13, 21, 26, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 14, 21, 26, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 13, 21, 26, 1, 0, DateTimeKind.Unspecified) });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "AspNetUsers",
                type: "nvarchar(13)",
                maxLength: 13,
                nullable: false,
                defaultValue: "");

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 14, 21, 19, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 13, 21, 19, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 14, 21, 19, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 13, 21, 19, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 14, 21, 19, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 13, 21, 19, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 14, 21, 19, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 13, 21, 19, 1, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "End time", "Start time" },
                values: new object[] { new DateTime(2024, 4, 14, 21, 19, 1, 0, DateTimeKind.Unspecified), new DateTime(2024, 4, 13, 21, 19, 1, 0, DateTimeKind.Unspecified) });
        }
    }
}
