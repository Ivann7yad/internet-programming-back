﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoList.DAL.Entity;
using ToDoList.DAL.Repository.Interfaces;
using ToDoList.DAL.Repository;
using Microsoft.AspNetCore.Mvc;
using ToDoList.DAL.Data;
using InternetPropgrammingBack.DAL.Repository.Interfaces;

namespace InternetPropgrammingBack.DAL.Repository
{
    public class StudentRepository: Repository<Student>, IStudentRepository
    {
        private readonly ApplicationDbContext _context;
        public StudentRepository([FromServices] ApplicationDbContext context)
            : base(context)
        {
            _context = context;
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
