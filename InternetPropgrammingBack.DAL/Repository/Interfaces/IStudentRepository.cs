﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoList.DAL.Entity;
using ToDoList.DAL.Repository.Interfaces;

namespace InternetPropgrammingBack.DAL.Repository.Interfaces
{
    public interface IStudentRepository: IRepository<Student>
    {
        void Save();
    }
}
