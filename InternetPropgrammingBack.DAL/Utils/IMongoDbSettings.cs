﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetPropgrammingBack.DAL.Utils
{
    public interface IMongoDbSettings
    {
        string MessagesCollectionName { get; set; }
        string GroupsCollectionName { get; set; }
        string IndividualChatsCollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}
