﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetPropgrammingBack.DAL.Utils
{
    public class MongoDbSettings: IMongoDbSettings
    {
        public string MessagesCollectionName { get; set; }
        public string GroupsCollectionName { get; set; }
        public string IndividualChatsCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}
