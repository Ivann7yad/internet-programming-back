﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDoList.DAL.Utility
{
    public class StaticData
    {
        public const string Male = "male";
        public const string Female = "female";

        public const string AliveStatus = "alive";
        public const string DeadStatus = "dead";

        public const string ToDoStatus = "to-do";
        public const string InProgressStatus = "in-progress";
        public const string DoneStatus = "done";

        public const string FeatureType = "feature";
        public const string BugType = "bug";
    }
}
